// ==UserScript==
// @name        lorify
// @namespace   https://bitbucket.org/b0r3d0m/lorify-userscript
// @description lorify provides you an autorefresh for threads and an easy way to view referenced comments on linux.org.ru's forums
// @include http*://*linux.org.ru/forum/*/*
// @include http*://*linux.org.ru/news/*/*
// @include http*://*linux.org.ru/gallery/*/*
// @include http*://*linux.org.ru/polls/*/* 
// @version     1.3.1
// @grant       none
// @require     http://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js
// @icon        http://icons.iconarchive.com/icons/icons8/christmas-flat-color/32/penguin-icon.png
// ==/UserScript==

var autorefreshTickIntervalMillisecs = 1000;

var autorefreshEnabled = true;
var autorefreshIntervalSecs = 10;
var autorefreshLeftSecs = autorefreshIntervalSecs;
var autorefreshLabel = null;
var newCommentsCount = 0;
var originalDocumentTitle = '';
var delayBeforePreviewMillisecs = 0;
var delayAfterPreviewMillisecs = 800;
var responsesAddingInProcess = false;
var commentsInfo = [];
var currentPreview = null;

function Comment(element, id, link, referencedCommentID, author) {
  this.element = element;
  this.id = id;
  this.link = link;
  this.referencedCommentID = referencedCommentID;
  this.author = author;
}

function getCommentInfo(comment) {
  var commentLinkElement = $(
    comment.find(
      'div.msg-container > div.msg_body > div.reply'
    ).find('a:contains(Ссылка)').first()
  );
  var commentLink = commentLinkElement.attr('href');
  if (typeof commentLink === 'undefined') {
    return null;
  }

  var matches = commentLink.match(/.*cid=(\d+).*/);
  if (matches === null) {
    return null;
  }
  var commentID = matches[1];

  var referencedCommentID = null;
  var a = comment.find('div.title > a').first();
  if (a.length > 0) {
    var referencedCommentLink = a.attr('href');
    if (typeof referencedCommentLink !== 'undefined') {
      var matches = referencedCommentLink.match(/.*cid=(\d+).*/);
      if (matches !== null) {
        referencedCommentID = matches[1];
      }
    }
  }

  var author = null;
  var creatorTag = comment.find('a[itemprop=creator]').first();
  if (creatorTag.length > 0) {
    author = creatorTag.text();
  } else {
    // anonymous
  }

  return new Comment(comment, commentID, commentLink, referencedCommentID, author);
}

function addCommentToCache(commentInfo) {
  var isNewComment = true;
  for (var i = 0; i < commentsInfo.length; ++i) {
    var cachedCommentInfo = commentsInfo[i];
    if (cachedCommentInfo.id === commentInfo.id) {
      commentsInfo[i] = cachedCommentInfo;
      isNewComment = false;
      break;
    }
  }
  if (isNewComment) {
    commentsInfo.push(commentInfo);
  }
}

function addResponsesLinksInternal(possibleAnswers) {
  for (var i = 0; i < possibleAnswers.length; ++i) {
    var comment = $(possibleAnswers[i]);
    var commentInfo = getCommentInfo(comment);
    if (commentInfo === null) {
      continue;
    }

    if (commentInfo.referencedCommentID !== null) {
      for (var j = 0; j < commentsInfo.length; ++j) {
        var cachedCommentInfo = commentsInfo[j];
        if (cachedCommentInfo.id === commentInfo.referencedCommentID) {
          if (cachedCommentInfo.element.find(
            'a.response[href="' + commentInfo.link + '"]'
          ).length > 0) {
            continue;
          }

          var responseTag = $('<a/>', {
            href: commentInfo.link,
            html: commentInfo.author ? commentInfo.author : '>>' + commentInfo.id,
            class: 'response',
            style: 'padding-left: 5px'
          });
          responseTag.click(function(e) {
            var comment = $('#comment-' + commentInfo.id).get(0);
            if (comment) {
              e.preventDefault();
              comment.scrollIntoView();
            }
          });

          var replyPanel = cachedCommentInfo.element.find('div.reply > ul').first();
          if (replyPanel.length === 0) {
            continue;
          }
          var responseBlock = replyPanel.find('.response-block').first();
          if (responseBlock.length === 0) {
            responseBlock = $('<li/>', {
              html: 'Ответы:',
              class: 'response-block'
            });
            replyPanel.append(' ').append(responseBlock);
          }

          setShowPreviewCallback(responseTag);
          responseBlock.append(responseTag);
        }
      }
    }

    addCommentToCache(commentInfo);
  }
}

function addResponsesLinks() {
  if (responsesAddingInProcess) {
    return;
  }
  responsesAddingInProcess = true;

  // Process current page
  var curPageComments = $('#comments').find('article.msg');
  addResponsesLinksInternal(curPageComments);

  // Process other pages
  var otherPagesRequests = [];
  do {
    var pagesNavBar = $('#comments').find('.nav').first();
    if (pagesNavBar.length === 0) {
      break;
    }

    var otherPagesLinksElements = pagesNavBar.find('a.page-number');
    for (var i = 0; i < otherPagesLinksElements.length; ++i) {
      var otherPageLinkElement = $(otherPagesLinksElements[i]);

      var otherPageLink = otherPageLinkElement.attr('href');
      if (typeof otherPageLink === 'undefined') {
        continue;
      }

      otherPagesRequests.push($.get(otherPageLink));
    }
  } while (false);

  if (otherPagesRequests.length === 0) {
    responsesAddingInProcess = false;
  } else {
    var defer = $.when.apply($, otherPagesRequests);
    defer.done(function() {
      $.each(arguments, function(index, responseData) {
        var possibleAnswers = $(responseData[0]).find('#comments').find('article.msg');
        addResponsesLinksInternal(possibleAnswers);
      });
    }).always(function() {
      responsesAddingInProcess = false;
    });
  }
}

function getOffset(element, offsetType) {
  var offset = 0;
  while (element) {
    offset += element[offsetType];
    element = element.offsetParent;
  }
  return offset;
}

function getScreenWidth() {
  return document.body.clientWidth || document.documentElement.clientWidth;
}

function getScreenHeight() {
  return  window.innerHeight || document.documentElement.clientHeight;
}

function removeElement(element) {
  if (!element) {
    return;
  }

  if (element.parentNode) {
    element.parentNode.removeChild(element);
  }
}

function removePreviews(e) {
  currentPreview = e.relatedTarget;
  if (!currentPreview) {
    return;
  }

  while (true) {
    if (/^preview/.test(currentPreview.id)) {
      break;
    } else {
      currentPreview = currentPreview.parentNode;
      if (!currentPreview) {
        break;
      }
    }
  }

  setTimeout(function() {
    if (currentPreview === null) {
      $('article.msg[id*="preview-"]').remove();
    } else {
      while (currentPreview.nextSibling) {
        if (!/^preview/.test(currentPreview.nextSibling.id)) {
          break;
        }
        removeElement(currentPreview.nextSibling);
      }
    }
  }, delayAfterPreviewMillisecs);
}

function showCommentInternal(commentElement, commentID, e) {
  currentPreview = commentElement.get(0);

  // Avoid duplicated IDs when the original comment was found on the same page
  commentElement.attr('id', 'preview-' + commentID);

  // From makaba
  var hoveredLink = e.target;
  var x = getOffset(hoveredLink, 'offsetLeft') + hoveredLink.offsetWidth / 2;
  var y = getOffset(hoveredLink, 'offsetTop');
  var screenWidth = getScreenWidth();
  var screenHeight = getScreenHeight();
  if (e.clientY < screenHeight * 0.75) {
    y += hoveredLink.offsetHeight;
  }
  commentElement.attr(
    'style',
      'position: absolute;' +
      // There are no limitations for the 'z-index' in the CSS standard,
      // so it depends on the browser. Let's just set it to 300
      'z-index: 300;' +
      'border: 1px solid grey;' +
      (
        x < screenWidth / 2
        ? 'left: ' + x
        : 'right: ' + parseInt(screenWidth - x + 2)
      ) + 'px;' +
      (
        e.clientY < screenHeight * 0.75
        ? 'top: ' + y
        : 'bottom: ' + parseInt(screenHeight - y - 4)
      ) + 'px;'
  );

  // If this comment contains link to another comment,
  // set the 'mouseover' hook to that 'a' tag
  var a = commentElement.find('div.title > a').first();
  if (a.length > 0) {
    setShowPreviewCallback(a);

    var referencedCommentID = -1;
    var referencedCommentLink = a.attr('href');
    if (typeof referencedCommentLink !== 'undefined') {
      var matches = referencedCommentLink.match(/.*cid=(\d+).*/);
      if (matches !== null) {
        referencedCommentID = matches[1];
      }
    }
    if (referencedCommentID !== -1) {
      a.click(function(e) {
        var comment = $('#comment-' + referencedCommentID).get(0);
        if (comment) {
          e.preventDefault();
          comment.scrollIntoView();
        }
      });
    }
  }

  commentElement.mouseenter(function() {
    if (!currentPreview) {
      currentPreview = this;
    }
  });

  commentElement.mouseleave(function(e) {
    removePreviews(e);
  });

  // Note that we append the comment to the '#comments' tag,
  // not the document's body
  // This is because we want to save the background-color and other styles
  // which can be customized by userscripts and themes
  $('#comments').find('article.msg').last().after(commentElement);
}

function findCachedComment(id) {
  for (var i = 0; i < commentsInfo.length; ++i) {
    var commentInfo = commentsInfo[i];
    if (commentInfo.id === id) {
      return commentInfo.element;
    }
  }
  return null;
}

function showPreview(e) {
  // Extract link to the comment
  var href = $(e.target).attr('href');
  if (typeof href === 'undefined') {
    return;
  }

  // Extract comment's ID from the 'href' attribute
  var matches = href.match(/.*cid=(\d+).*/);
  if (matches === null) {
    return;
  }
  var commentID = matches[1];

  // Let's reduce an amount of GET requests
  // by searching a cache of comments first
  var commentElement = findCachedComment(commentID);
  if (commentElement !== null) {
    showCommentInternal(
      // Without the 'clone' call we'll just move the original comment
      commentElement.clone(true),
      commentID,
      e
    );
    return;
  }

  // Get an HTML containing the comment
  $.get(href, function(data) {
    // Search for the comment on the requested page
    var commentElementSelector = 'article.msg[id=comment-' + commentID + ']';
    var commentElement = $(data).find(
      commentElementSelector
    ).first();
    if (commentElement.length === 0) {
      return;
    }

    showCommentInternal(
      commentElement,
      commentID,
      e
    );
  });
}

function setShowPreviewCallback(elements) {
  elements.hover(function(e) {
    $(this).data('timeout',
      window.setTimeout(
        function() {
          showPreview(e);
        }, delayBeforePreviewMillisecs
      )
    );
  }, function(e) {
    clearTimeout($(this).data('timeout'));
    removePreviews(e);
  });
}

function autorefreshTick() {
  if (!autorefreshEnabled) {
    setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs);
    return;
  }

  autorefreshLeftSecs -= 1;
  if (autorefreshLeftSecs !== 0) {
    autorefreshLabel.find('span').text(
      'Автообновление через ' + autorefreshLeftSecs + ' с...'
    );
    setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs);
    return;
  }

  autorefreshLabel.find('span').text(
    'Обновление...'
  );

  var comments = $('#comments');

  $.get(document.location.href, function(data) {
    var messages = $(data).find('article.msg');
    for (var i = 0; i < messages.length; ++i) {
      var message = $(messages[i]);
      var messageID = message.attr('id');
      var isNewMessage = $('#' + messageID).length === 0;
      if (!isNewMessage) {
        continue;
      }

      ++newCommentsCount;

      var a = message.find('div.title > a').first();
      if (a.length > 0) {
        setShowPreviewCallback(a);
        var referencedCommentID = -1;
        var referencedCommentLink = a.attr('href');
        if (typeof referencedCommentLink !== 'undefined') {
          var matches = referencedCommentLink.match(/.*cid=(\d+).*/);
          if (matches !== null) {
            referencedCommentID = matches[1];
          }
        }
        if (referencedCommentID !== -1) {
          a.click(function(e) {
            var comment = $('#comment-' + referencedCommentID).get(0);
            if (comment) {
              e.preventDefault();
              comment.scrollIntoView();
            }
          });
        }
      }

      comments.append(message);
    }

    // Remove all nav. bars related to the pagination
    // (there are also nav. bars related to threads, but let's just skip them)
    var pagesBefore = 0;
    var currentNavBars = $(document.body).find('.nav');
    for (var i = 0; i < currentNavBars.length; ++i) {
      var currentNavBar = $(currentNavBars[i]);
      var pagesElementsCount = currentNavBar.find('.page-number').length;
      var isPagesNavBar = pagesElementsCount !== 0;
      if (isPagesNavBar) {
        pagesBefore = pagesElementsCount;
        currentNavBar.remove();
      }
    }

    var pagesAfter = 0;
    var newPagesNavBar = $(data).find('#comments').find('.nav').first();
    if (newPagesNavBar.length === 0) {
      comments.append(autorefreshLabel);
    } else {
      pagesAfter = newPagesNavBar.find('.page-number').length;
      comments.prepend(newPagesNavBar.clone());
      comments.append(autorefreshLabel);
      comments.append(newPagesNavBar.clone());
    }

    if (pagesAfter > pagesBefore) {
      document.title = '(!) ' + originalDocumentTitle;
    } else if (newCommentsCount !== 0) {
      document.title = '(' + newCommentsCount + ') ' + originalDocumentTitle;
    }

    addResponsesLinks();
  }).always(function() {
    autorefreshLeftSecs = autorefreshIntervalSecs;
    autorefreshLabel.find('span').text(
      'Автообновление через ' + autorefreshLeftSecs + ' с...'
    );

    setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs);
  });
}

function onAutorefreshCheckboxChange() {
  autorefreshEnabled = $(this).is(':checked');
  if (!autorefreshEnabled) {
    autorefreshLabel.find('span').text(
      'Автообновление'
    );
  } else {
    autorefreshLeftSecs = autorefreshIntervalSecs;
    autorefreshLabel.find('span').text(
      'Автообновление через ' + autorefreshLeftSecs + ' с...'
    );
  }
}

function setAutorefresher() {
  originalDocumentTitle = document.title;

  autorefreshLabel = $('<label/>', {
    // Without the inner 'span' tag
    // the next call to the 'text' function will remove the 'checkbox' control
    html: autorefreshEnabled
      ? '<span>Автообновление через '
        + autorefreshLeftSecs
        + ' с...</span>'
      : '<span>Автообновление</span>'
  });
  var autorefreshCheckbox = $('<input/>', {
    type: 'checkbox',
    checked: autorefreshEnabled
  });
  autorefreshCheckbox.change(onAutorefreshCheckboxChange);
  autorefreshLabel.prepend(autorefreshCheckbox);
  $('#comments').append(autorefreshLabel);

  $(window).scroll(function() {
    if (newCommentsCount !== 0) {
      document.title = originalDocumentTitle;
      newCommentsCount = 0;
    }
  });

  // setInterval calls may overlap so let's call setTimeot each time manually
  setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs);
}

function removePreviewsOnClick() {
  $(window).click(function() {
    var previewSelector = 'article.msg[id*="preview-"]';
    if (currentPreview === null) {
      $(previewSelector).remove();
    }
  });
}

var archivedTopicString = 'Вы не можете добавлять комментарии в эту тему. Тема перемещена в архив.';
var deletedTopicString = 'Вы не можете добавлять комментарии в эту тему. Тема удалена.';

function disableAutorefreshForClosedTopics() {
  var infoblock = $('.infoblock').text();

  if (infoblock.indexOf(archivedTopicString) !== -1 ||
      infoblock.indexOf(deletedTopicString) !== -1) {
    autorefreshEnabled = false;
  }
}

function onDOMReady() {
  removePreviewsOnClick();
  setShowPreviewCallback($('div.title > a'));
  disableAutorefreshForClosedTopics();
  setAutorefresher();
  addResponsesLinks();
}

$(function() {
  onDOMReady();
});
